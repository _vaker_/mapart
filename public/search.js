let maps;

onmessage = (event)=>{
    const {data} = event;
    if(data.maps) maps = data.maps;
    if(data.search != undefined){
        var mapResults = [];
        if(data.search.length == 0) return self.postMessage(maps);
        for(map of maps){
            const queryWords = data.search.toLowerCase().split(' ');
            var match = true;
            for(word of queryWords){
                if(!map.name.toLowerCase().includes(word) && !map.authors.join(', ').toLowerCase().includes(word)) match = false;
            }
            if(match) mapResults.push(map);
        }
        self.postMessage(mapResults);
    }
}