// This is meant to be run in nodejs to sort maps.json by alphabetical order.
const fs = require('fs');

const maps = require('./maps.json');

maps.sort((a, b) => a.name.localeCompare(b.name));

const mapTiles = [];
maps.forEach(map=>{
    for(let tile of map.maps){
        mapTiles.push(tile.id.toString());
    }
});

fs.promises.readdir('maps/').then(files=>{
    for(let tile of mapTiles){
        if(!files.includes(tile + '.bin')) console.log('Missing map file for', tile);
    }
    for(let file of files){
        let id = file.split('.').shift();
        if(!mapTiles.includes(id)) console.log('Unused map file', file);
    }
});

console.log('Total maps:', maps.length);

const mapsOutJson = JSON.stringify(maps, null, 4);
fs.writeFile('maps.json', mapsOutJson, ()=>{});
